function data()
return {
	en = {
		
["mod_name"] = "DB Catenary",

["mod_desc"] = "Adds an Austrian Catenary with placable Assets.",


["DB Oberleitunstoolbox - Quertragewerke"] = "DB catenarytoolbox - Quertragewerke",
["DB Oberleitunstoolbox - Rohrausleger"] = "DB catenarytoolbox - Rohrausleger",
["DB Oberleitunstoolbox - Einzelmasten"] = "DB catenarytoolbox - normal Masts",
["Diverse DB Oberleitungsbauten als Asset."] = "DB catenary as assets.",

-- Allgemein
["Oberleitungsbauart"]  = "Catenary type",
["Modern"]  = "modern",
["Alt"]  = "old",
["Start"]  = "start",
["Mitte"]  = "repeat",
-- Quertragwerk
["Quertragwerk Einzelteile"]  = "seperatly components",
["Quertragwerk fuer X Gleise"]  = "Quertragwerk for X tracks",
-- Einzelmasten
["Einzelner Mast"]  = "seperatly mast",
["2 Gleise"]  = "2 tracks",
["Doppelausleger"]  = "double Ausleger",
["Standard"]  = "default",
["Einzelner Abspannmast"]  = "track end mast",
-- Rohrausleger
["Rohrausleger Einzelausleger"]  = "Rohrausleger seperatly Ausleger",
["Mast + Ausleger Aussen"]  = "Mast + Ausleger outside",
["Rohrausleger 2 Gleise"]  = "2 tracks",
["4 Gleise"]  = "4 tracks",
["Offset Ausleger Aussen"]  = "Offset Ausleger outside",
["Rotation Mast + Ausleger Aussen"]  = "Rotation Ausleger outside",

},
de = {

["mod_name"] = "DB Oberleitung",

["mod_desc"] = "Fügt eine DB Oberleitung hinzu mit Assets zum selbst platzieren.",

["DB Oberleitunstoolbox - Quertragewerke"] = "DB Oberleitunstoolbox - Quertragewerke",
["DB Oberleitunstoolbox - Rohrausleger"] = "DB Oberleitunstoolbox - Rohrausleger",
["DB Oberleitunstoolbox - Einzelmasten"] = "DB Oberleitunstoolbox - Einzelmasten",
["Diverse DB Oberleitungsbauten als Asset."] = "Diverse DB Oberleitungsbauten als Asset.",

-- Allgemein
["Oberleitungsbauart"]  = "Oberleitungsbauart",
["Modern"]  = "Modern",
["Alt"]  = "Alt",
["Start"]  = "Start",
["Mitte"]  = "Mitte",
-- Quertragwerk
["Quertragwerk Einzelteile"]  = "Quertragwerk Einzelteile",
["Quertragwerk fuer X Gleise"]  = "Quertragwerk fuer X Gleise",
-- Einzelmasten
["Einzelner Mast"]  = "Einzelner Mast",
["2 Gleise"]  = "2 Gleise",
["Doppelausleger"]  = "Doppelausleger",
["Standard"]  = "Standard",
["Einzelner Abspannmast"]  = "Einzelner Abspannmast",
-- Rohrausleger
["Rohrausleger Einzelausleger"]  = "Rohrausleger Einzelausleger",
["Mast + Ausleger Aussen"]  = "Mast + Ausleger Aussen",
["Rohrausleger 2 Gleise"]  = "Rohrausleger 2 Gleise",
["4 Gleise"]  = "4 Gleise",
["Offset Ausleger Aussen"]  = "Offset Ausleger Aussen",
["Rotation Mast + Ausleger Aussen"]  = "Rotation Mast + Ausleger Aussen",


},

}
end
function data()
  return {
  
	info = {
		minorVersion = 0,
		severityAdd = "NONE",
		severityRemove = "NONE", 
		name = _("mod_name"),
		description = _("mod_desc"),


		authors = {
			{
				name = "Ribase",
				role = "CREATOR",
				text = 'Edit, Rework/Refactor',
			},
			{
				name = "Taurus1116Fan",
				role = "CREATOR",
				text = 'Idee',
			},
			{
				name = 'BR146',
				role = 'CREATOR',
				text = '3D-Modell, Idee',
				steamProfile = '76561198146230194',
				tfnetId = '18313'
			},
			{
				name = 'Bandion',
				role = 'CO-CREATOR',
				text = 'Textur',
				tfnetId = '21221'
			},
		},
		tags = { "Europe", "Track" },
		visible = true,	
	},
  
  }
end
